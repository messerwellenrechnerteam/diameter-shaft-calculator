﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DiameterShaftCalculator.Domain;

namespace DiameterShaftCalculator.Utils
{
    public class GenericTemporarySerializer<T>
    {
        public virtual string FilePath { get; set; }

        public void Write(T objectToWrite, string fileName, bool append = false)
        {
            if (!Directory.Exists(FilePath))
            {
                var di = Directory.CreateDirectory(FilePath);
            }


            using (Stream stream = File.Open(FilePath + fileName, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        public T Read(string fileName)
        {
            try
            {
                using (Stream stream = File.Open(FilePath + fileName, FileMode.Open))
                {
                    var binaryFormatter = new BinaryFormatter();
                    return (T) binaryFormatter.Deserialize(stream);
                }
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }

    public class SettingsSerializer : GenericTemporarySerializer<ApplicationSetting>
    {
        public override string FilePath
        {
            get => Path.GetTempPath() + "\\Messerwellenrechner\\Einstellungen\\";
            set => Debug.WriteLine("Setter not supportet. Path was not changed to " + value);
        }
    }
}