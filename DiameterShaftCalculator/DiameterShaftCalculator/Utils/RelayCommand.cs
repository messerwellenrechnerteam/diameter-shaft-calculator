﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DiameterShaftCalculator.Utils
{
    // RelayCommand objects delegate to the methods passed
    // as an argument.
    public class RelayCommand : ICommand
    {
        protected readonly Predicate<object> CanExecutePredicate;
        protected readonly Action<object> ExecuteAction;

        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            ExecuteAction = execute ?? throw new ArgumentNullException(nameof(execute));
            CanExecutePredicate = canExecute;
        }

        // Invoked when command is executed
        public virtual void Execute(object parameter)
        {
            try
            {
                ExecuteAction(parameter);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        // The returned value indicates whether command is active (can be executed) or not.
        public bool CanExecute(object parameter)
        {
            return CanExecutePredicate?.Invoke(parameter) ?? true;
        }

        // Occurs when changes occur that affect whether or not the command should execute.
        // The handling of this event is delegated to the CommandManger.
        // WPF invokes CanExecute in response to this event. CanExecute returns the
        // command's state.
        public event EventHandler CanExecuteChanged
        {
            add => CommandManager.RequerySuggested += value;
            remove => CommandManager.RequerySuggested -= value;
        }
    }

    public class AsyncRelayCommand : RelayCommand
    {
        public AsyncRelayCommand(Action<object> execute) : base(execute)
        {
        }

        public AsyncRelayCommand(Action<object> execute, Predicate<object> canExecute) : base(execute, canExecute)
        {
        }

        public override async void Execute(object parameter)
        {
            await Task.Run(() => { base.Execute(parameter); });
        }
    }
}