﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DiameterShaftCalculator.Utils
{
    public class ColorInfo : IEquatable<ColorInfo>
    {
        public string ColorName { get; set; }
        public Color Color { get; set; }

        public SolidColorBrush Brush => new SolidColorBrush(Color);

        public string HexValue => Color.ToString();

        public ColorInfo(string colorName, Color color)
        {
            ColorName = colorName;
            Color = color;
        }

        public bool Equals(ColorInfo other) {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(ColorName, other.ColorName) && Color.Equals(other.Color);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((ColorInfo) obj);
        }

        public override int GetHashCode() {
            unchecked {
                return ((ColorName != null ? ColorName.GetHashCode() : 0) * 397) ^ Color.GetHashCode();
            }
        }
    }

    public class Colormanager
    {
        private static readonly IList<ColorInfo> Colors = new List<ColorInfo>() {
            new ColorInfo("Rot", System.Windows.Media.Colors.Red),
            new ColorInfo("Blau", System.Windows.Media.Colors.Blue),
            new ColorInfo("Grün", System.Windows.Media.Colors.Green),
            new ColorInfo("Orange", System.Windows.Media.Colors.Orange),
            new ColorInfo("Violett", System.Windows.Media.Colors.BlueViolet),
            new ColorInfo("Schwarz", System.Windows.Media.Colors.Black),
            new ColorInfo("Pink", System.Windows.Media.Colors.DeepPink),
            new ColorInfo("Gelb", System.Windows.Media.Colors.Yellow),
            new ColorInfo("Türkis", System.Windows.Media.Colors.Cyan),
            new ColorInfo("Hellgrün", System.Windows.Media.Colors.Lime),
        };

        private static int _currentidx = 0;
        private static int Currentidx {
            get {
                var i = _currentidx;
                _currentidx++;
                if (_currentidx >= Colors.Count) _currentidx = 0;
                return i;
            }
        }

        public static ColorInfo NextColor()
        {
            return Colors[Currentidx];
        }

        public static IList<ColorInfo> GetColors() {
            return Colors;
        }
    }
}
