﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DiameterShaftCalculator.Utils {
    public static class Utility {
        public static T DeepClone<T>(T obj) {
            using (var ms = new MemoryStream()) {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T)formatter.Deserialize(ms);
            }
        }

        public static void AddAll<T>(this ICollection<T> target, IEnumerable<T> source) {
            foreach (var elem in source) {
                target.Add(elem);
            }
        }

        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

        public static IEnumerable<T> GetRange<T>(this ICollection<T> source, int start = 0, int range = -1) {
            if (range < 0) {
                range = source.Count - start;
            }

            return source.Skip(start).Take(range);
        }

        public static double RoundUpToPointFive(double value) {
            return Math.Ceiling(value * 2) / 2;
        }
    }
}
