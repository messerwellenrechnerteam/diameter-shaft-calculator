﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DiameterShaftCalculator.Properties;

namespace DiameterShaftCalculator.Utils
{
   

    public class ObservableMulitSet<T> : Dictionary<T, int>, INotifyCollectionChanged, INotifyPropertyChanged where T : IEquatable<T>{
     
        public ObservableMulitSet(int limit){
           Limit = limit;
        }

        public ObservableMulitSet(IEnumerable<T> collection)
        {
            foreach (var item in collection)
                Add(item);
        }

        public ObservableMulitSet(List<T> list)
        {
            foreach (var item in list)
                Add(item);
        }

        public int Limit { get; protected set; }
        
        public new virtual void Clear() {
            base.Clear();
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public virtual void Add(T item){
            if (ContainsKey(item))
            {
                if (this[item] <= Limit) this[item]++;
            }
            else {
               this.Add(item, 1);
            }
           OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public new virtual T Remove(T item){
            if (!ContainsKey(item)) return default(T);

            this[item]--;
            if (this[item] <= 0) base.Remove(item);
            return item;
        }

        public virtual event NotifyCollectionChangedEventHandler CollectionChanged;


        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e) {
            RaiseCollectionChanged(e);
            RaisePropertyChanged(new PropertyChangedEventArgs("Count"));
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        protected virtual event PropertyChangedEventHandler PropertyChanged;


        private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs e) {
            CollectionChanged?.Invoke(this, e);
        }

        private void RaisePropertyChanged(PropertyChangedEventArgs e) {
            PropertyChanged?.Invoke(this, e);
        }


        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged {
            add => PropertyChanged += value;
            remove => PropertyChanged -= value;
        }
    }

    public class ObservableSet<T> : ObservableMulitSet<T> where T : IEquatable<T>{
        public ObservableSet() : base(1){}
    }
}
