﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.VM;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class YPositionConverter : IMultiValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null || value.GetType() != typeof(OrderVm)) return 0.0;

            var order = (OrderVm) value;
            var applicationVm = order.ApplicationVm;

            var idx = applicationVm.Orders.IndexOf(order);
            var multiplyer = idx % 2 == 0 ? -1.0 : 1.0;

            return Math.Floor(idx/2.0) * multiplyer;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2 && values.Length != 3) return default(Double);
            if (values[0] == null || values[0].GetType() != typeof(double)) return default(Double);
            if (values[1] == null || values[1].GetType() != typeof(OrderVm)) return default(Double);

            var canvasHeight = (double)values[0];
            var orderVm = (OrderVm)values[1];
            Double y = 0.0;
            y += canvasHeight / 2;

            var applicationVm = orderVm.ApplicationVm;

            var idx = applicationVm.Orders.IndexOf(orderVm);
            var multiplyer = idx % 2 == 0 ? -1.0 : 1.0;

            y += (Math.Floor(idx / 2.0)+1) * (canvasHeight / 30) * multiplyer;
            if (values.Length != 3) return y;
            if (multiplyer < 0)
            {
                y -= 24;
            }
            else
            {
                y -= 2;
                if (values.Length != 3) return y;
                if ((double)values[2] <= ApplicationSetting.MinRollWidthForDisplayingTextHorizontally) y += 4;
            }
            return y;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture){
            throw new NotImplementedException();
        }
    }
}
