﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Data;
using DiameterShaftCalculator.Domain;
namespace DiameterShaftCalculator.Utils.Converter
{
    public class CanvasPositionConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
           
            if (value.Where((t, i) => value?[i] == null || t.GetType() != typeof(Double)).Any()){
                return 0;
            }

            // value[0] = virtual machine width (= canvas width)
            // value[1] = real knife position

            var res = ((double) value[0] * (double)value[1]) / ApplicationSetting.MachineWidth ;
            if (value.Length == 3 && value[2] != null){
                res += (double) value[2];
            }

            if (value.Length == 2 || value[2] == null) return res;
            return res - ((double)value[2]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}