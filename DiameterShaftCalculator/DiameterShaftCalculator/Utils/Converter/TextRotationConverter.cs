﻿using System;
using System.Globalization;
using System.Windows.Data;
using DiameterShaftCalculator.Domain;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class TextRotationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return 0.0;
            var val = (double) value;
            return val <= ApplicationSetting.MinRollWidthForDisplayingTextHorizontally ? 90.0 : 0.0;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
