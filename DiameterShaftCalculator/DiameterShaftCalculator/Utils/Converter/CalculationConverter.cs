﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class CalculationConverter : IMultiValueConverter, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var stringValue = (string) parameter;

            var result = (double) value;
            if (stringValue == null) return result;
            var sign = stringValue.Substring(0, 1);
            var number = System.Convert.ToDouble(stringValue.Substring(1));
            switch (sign)
            {
                case "*":
                    result = result * number;
                    break;
                case "/":
                    result = result / number;
                    break;
                case "+":
                    result = result + number;
                    break;
                case "-":
                    result = result - number;
                    break;
                default:
                    result = result + number;
                    break;
            }
            //Debug.WriteLine("Calculation Converter: " + sign + " " + number + "    Result: " + result);
            return result;
        }

        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }



        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {

            var stringValues = values.Aggregate("", (current, value) => current + value.ToString());

            var result = 0.0;
            if (string.IsNullOrEmpty(stringValues)) return result;

            Debug.WriteLine(stringValues);

            var sc = new MSScriptControl.ScriptControl {Language = "VBScript"};
            try {
                result = sc.Eval(stringValues);
                return result;
            }
            catch (Exception) {
                return 0.0;
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}