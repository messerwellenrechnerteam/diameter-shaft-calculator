﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Windows.Data;
using DiameterShaftCalculator.VM;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class RollWidthsToSetConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value.GetType() != typeof(ObservableCollection<RollVm>)) return null;
            var input = (ObservableCollection<RollVm>) value;

            var tempresult = new Dictionary<double, int>();
            foreach (var roll in input){
                if (tempresult.ContainsKey(roll.Width))
                {
                    tempresult[roll.Width]++;
                }
                else
                {
                    tempresult.Add(roll.Width, 1);
                }
            }

            var result = new StringBuilder();
            foreach (var rollWidth in tempresult){
                result.Append(rollWidth.Value + "x " + rollWidth.Key);
                result.Append(", ");
            }
            if(result.Length > 3) result.Remove(result.Length - 2, 2);

            return result.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
