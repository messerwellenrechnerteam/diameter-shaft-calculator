﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using DiameterShaftCalculator.Domain;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class MinWidthColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            var val = (string) value;
            if (!double.TryParse(val, out double result)) return new ColorInfo("Schwarz", Colors.Black);
            return result < ApplicationSetting.MinKnifeDistance ? new ColorInfo("Rot", Colors.Red) : new ColorInfo("Schwarz", Colors.Black);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
