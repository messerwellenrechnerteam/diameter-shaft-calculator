﻿using System;
using System.Globalization;
using System.Windows.Data;
using DiameterShaftCalculator.VM;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class ColorConverter : IMultiValueConverter
    {
        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture){
            var res = new ColorInfo("Schwarz", System.Windows.Media.Colors.Black).Brush;
            if (value == null || value.Length == 0 || value[0] == null) return res;
            if(value.Length == 3 && value[2] != null) return new ColorInfo("Schwarz", System.Windows.Media.Colors.Black).Brush;
            if (value[0].GetType() == typeof(OrderVm)) return ConvertOrderVm((OrderVm)value[0]);
            if (value[0].GetType() == typeof(KnifeVm)) return ConvertKnifeVm((KnifeVm)value[0]);

            return res;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        private object ConvertOrderVm(OrderVm orderVm){
            var color = orderVm.Color.Brush;
            var applicationVm = orderVm.ApplicationVm;
            if (applicationVm.CurrentOrder == null) return color;
            if (applicationVm.CurrentOrder != orderVm)
            {
                color = new ColorInfo("Grau", System.Windows.Media.Colors.Gray).Brush;
            }
            return color;
        }

        private object ConvertKnifeVm(KnifeVm knifeVm)
        {
            var color = knifeVm.Color.Brush;
            if (!knifeVm.IsNotVirtual)
            {
                return new ColorInfo("Transparent", System.Windows.Media.Colors.Transparent).Brush;
            }
            var applicationVm = knifeVm.OrderVm.ApplicationVm;
            if (applicationVm.CurrentOrder == null) return color;
            if (applicationVm.CurrentOrder != knifeVm.OrderVm)
            {
                color = new ColorInfo("Grau", System.Windows.Media.Colors.Gray).Brush;
            }
            return color;
        }
    }
}
