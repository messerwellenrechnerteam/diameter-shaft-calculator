﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class GlobalColorConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) {
            // value[0] = GlobalColor
            
            if(values == null || values.Length != 2) return new ColorInfo("Schwarz", Colors.Black).Brush;
            return values[0] != null ? ((ColorInfo) values[0]).Brush : ((ColorInfo)values[1]).Brush;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
}
