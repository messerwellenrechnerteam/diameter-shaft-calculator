﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using DiameterShaftCalculator.Domain;

namespace DiameterShaftCalculator.Utils.Converter
{
    public class NumberOfKnifesConverter : IValueConverter{
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var res =  new ColorInfo("Schwarz", Colors.Black).Brush;
            if (value == null) return res;
            return (int) value > ApplicationSetting.MaxNumberOfKnifes ? new ColorInfo("Rot", Colors.Red).Brush : res;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
