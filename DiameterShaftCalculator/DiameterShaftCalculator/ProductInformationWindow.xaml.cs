﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;

namespace DiameterShaftCalculator
{
    /// <summary>
    ///     Interaktionslogik für ProductInformationWindow.xaml
    /// </summary>
    public partial class ProductInformationWindow : Window
    {
        private static bool _open = false;

        private ProductInformationWindow()
        {
            Closed += (sender, args) => { _open = false; };
            InitializeComponent();
        }

        public static void ShowInstance()
        {
            if (_open) return;
            new ProductInformationWindow().Show();
            _open = true;
        }

        private void ProductInformationWindow_OnClosed(object sender, EventArgs e){
            Hide();
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
    }
}