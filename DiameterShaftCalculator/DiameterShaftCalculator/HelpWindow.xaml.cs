﻿using System;
using System.ComponentModel;
using System.Windows;
using DiameterShaftCalculator.VM;

namespace DiameterShaftCalculator
{
    /// <summary>
    ///     Interaktionslogik für HelpWindow.xaml
    /// </summary>
    public partial class HelpWindow : Window
    {
        private static bool _open = false;

        private HelpWindow()
        {
            Closed += (sender, args) => { _open = false; };
            Loaded += (sender, args) => { DataContext = new HelpVm(); };
            InitializeComponent();
        }

        public static void ShowInstance(){
            if (_open) return;
            new HelpWindow().Show();
            _open = true;
        }

        private void HelpWindow_OnClosed(object sender, EventArgs e){
            Hide();
        }
    }
}