﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.Utils;
using DiameterShaftCalculator.VM;

namespace DiameterShaftCalculator
{
    /// <summary>
    ///     Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ApplicationVm applicationVm;
        public MainWindow()
        {
            InitializeComponent();

            var ser = new SettingsSerializer();
            var x = ser.Read(ApplicationSetting.SettingsFilePath);
            var noSettings = false;
            if (x != null) ApplicationSetting.Set(x);
            else{
                noSettings = true;
            }

            Loaded += (s, e) => {
                applicationVm = new ApplicationVm(DrawCanvas, this);
                var context = applicationVm;
                this.KeyDown += MainWindow_KeyDown;
                this.KeyUp += MainWindow_KeyUp;
                DataContext = context; 
                if(noSettings) SettingsWindow.ShowInstance(context);
                if(ApplicationSetting.ShowHelpWindowAtStart) HelpWindow.ShowInstance();
            };
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.LeftCtrl && e.Key != Key.RightCtrl) return;
            applicationVm.MoveLeftKey = "<<";
            applicationVm.MoveRightKey = ">>";
        }

        private void MainWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.LeftCtrl && e.Key != Key.RightCtrl) return;
            applicationVm.MoveLeftKey = "<";
            applicationVm.MoveRightKey = ">";
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Struct representing a point.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Point(POINT point)
            {
                return new Point(point.X, point.Y);
            }
        }

        /// <summary>
        /// Retrieves the cursor's position, in screen coordinates.
        /// </summary>
        /// <see>See MSDN documentation for further information.</see>
        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out POINT lpPoint);

        public static Point GetCursorPosition()
        {
            POINT lpPoint;
            GetCursorPos(out lpPoint);

            return lpPoint;
        }
    }
}