﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.VM;

namespace DiameterShaftCalculator
{
    /// <summary>
    ///     Interaktionslogik für SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;

        private static SettingsWindow _currentWindow;
        private SettingsWindow(ApplicationVm applicationVm)
        {
            InitializeComponent();
            InitializeOldValues();
            Loaded += (s, e) =>
            {
                DataContext = new SettingsVm(this, applicationVm);
                var hwnd = new WindowInteropHelper(this).Handle;
                SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
            };
        }

        private void InitializeOldValues() {
            OldMaxNumberOfKnifes = ApplicationSetting.MaxNumberOfKnifes;
            OldMinKnifeDistance = ApplicationSetting.MinKnifeDistance;
            OldMachineCenter = ApplicationSetting.MachineCenter;
            OldMaxRolls = ApplicationSetting.MaxRolls;
            OldDefaultRollWidth = ApplicationSetting.DefaultRollWidth;
            OldIsStartingLeft = ApplicationSetting.IsStartingLeft;
            OldShowHelpWindowAtStart = ApplicationSetting.ShowHelpWindowAtStart;
            OldSavePrintBlackWhite = ApplicationSetting.SavePrintBlackWhite;
        }

        public int OldMaxNumberOfKnifes { get; private set; }

        public double OldMinKnifeDistance { get; private set; }

        public double OldMachineCenter { get; private set; }

        public double OldDefaultRollWidth { get; private set; }

        public int OldMaxRolls { get; private set; }

        public bool OldIsStartingLeft { get; private set; }

        public bool OldShowHelpWindowAtStart { get; private set; }
        public bool OldSavePrintBlackWhite { get; private set; }

        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        public static void ShowInstance(ApplicationVm applicationVm) {
            if (_currentWindow == null) _currentWindow = new SettingsWindow(applicationVm);
            if (_currentWindow.IsActive) return;
            _currentWindow.InitializeOldValues();
            _currentWindow.Show();
        }
    }
}