﻿namespace DiameterShaftCalculator.Domain
{
    public enum CutTypeEnum
    {
        Beidseitig,
        Mitteschneider,
        Links,
        Rechts
    }
}