﻿using System;
using DiameterShaftCalculator.Utils;

namespace DiameterShaftCalculator.Domain
{
    public class Knife : IEquatable<Knife>
    {
        public Knife(double realPosition, bool isVirtual = false)
        {
            IsVirtual = isVirtual;
            RealPosition = realPosition;
        }

        public bool IsVirtual { get; set; }
        public double RealPosition { get; set; }

        public void Move(double offset)
        {
            var o = Utility.RoundUpToPointFive(offset);
            RealPosition += o;
            RealPosition = Utility.RoundUpToPointFive(Math.Max(Math.Min(ApplicationSetting.MachineCenter * 2, RealPosition),0));
        }


        public bool Equals(Knife other){
            if (ReferenceEquals(null, other)) return false;
            return ReferenceEquals(this, other) || RealPosition.Equals(other.RealPosition);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((Knife) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = IsVirtual.GetHashCode();
                hashCode = (hashCode * 397) ^ RealPosition.GetHashCode();
                return hashCode;
            }
        }
    }
}