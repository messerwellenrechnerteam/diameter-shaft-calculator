﻿using System;
using System.Runtime.Serialization;

namespace DiameterShaftCalculator.Domain
{
    [Serializable]
    [DataContract]
    public class ApplicationSetting
    {
        private ApplicationSetting(int maxNumberOfKnifes, double minKnifeDistance,
            double machineCenter, int maxRolls, double defaultRollWidth, bool isStartingLeft, bool showHelpWindowAtStart, bool savePrintBlackWhite)
        {
            _maxNumberOfKnifesWrapper = maxNumberOfKnifes;
            _minKnifeDistanceWrapper = minKnifeDistance;
            _machineCenterWrapper = machineCenter;
            _maxRolls = maxRolls;
            _defaultRollWidth = defaultRollWidth;
            _isStartingLeft = isStartingLeft;
            _showHelpWindowAtStart = showHelpWindowAtStart;
            _savePrintBlackWhite = savePrintBlackWhite;
        }

        [DataMember(Name = "MaxNumberOfKnifes")]
        private int _maxNumberOfKnifesWrapper;
        public int _maxNumberOfKnifes
        {
            get => _maxNumberOfKnifesWrapper;
            private set => _maxNumberOfKnifesWrapper = value < 1 ? 1 : value;
        }

        [DataMember(Name = "MinKnifeDistance")]
        private double _minKnifeDistanceWrapper;
        public double _minKnifeDistance {
            get => _minKnifeDistanceWrapper;
            private set => _minKnifeDistanceWrapper = value < 1 ? 1 : value;
        }

        [DataMember(Name = "MachineCenter")]
        private double _machineCenterWrapper;
        public double _machineCenter {
            get => _machineCenterWrapper;
            private set => _machineCenterWrapper = value < 1 ? 1 : value;
        }

        [DataMember(Name = "MaxNumberOfRolls")]
        private int _maxRollsWrapper = 30;
        public int _maxRolls {
            get => _maxRollsWrapper;
            private set => _maxRollsWrapper = value < 1 ? 1 : value;
        }

        [DataMember(Name = "DefaultRollWidth")]
        private double _defaultRollWidthWrapper;
        public double _defaultRollWidth {
            get => _defaultRollWidthWrapper;
            private set {
                _defaultRollWidthWrapper = value < 1 ? 1 : value;
                _defaultRollWidthWrapper = value > _machineCenterWrapper * 2 ? _machineCenterWrapper * 2 : value;
            }
        }

        [DataMember(Name = "IsStartingLeft")]
        public bool _isStartingLeft { get; private set; } = true;

        [DataMember(Name = "ShowHelWindowAtStart")]
        public bool _showHelpWindowAtStart { get; private set; } = true;

        [DataMember(Name = "SavePrintBlackWhite")]
        public bool _savePrintBlackWhite { get; private set; } = true;


        // ------------------------------------------------------------------------------
        // ------------------------------------------------------------------------------
        /***
         * Singleton 
         */
        public static readonly string SettingsFilePath = "settings.dscsetting";

        static ApplicationSetting()
        {
            _current = new ApplicationSetting(-1, 1, -1, -1, -1, true, true, true);
        }

        private static ApplicationSetting _current;

        public static int MaxNumberOfKnifes {
            get => _current._maxNumberOfKnifes;
            set => _current._maxNumberOfKnifes = value;
        }

        public static double DefaultRollWidth
        {
            get => _current._defaultRollWidth;
            set => _current._defaultRollWidth = value;
        }

        public static double MinKnifeDistance
        {
            get => _current._minKnifeDistance;
            set => _current._minKnifeDistance = value;
        }

        public static double MachineCenter
        {
            get => _current._machineCenter;
            set => _current._machineCenter = value;
        }

        public static double MachineWidth
        {
            get => _current._machineCenter * 2;
            set => _current._machineCenter = value / 2.0;
        }

        // readonly setting
        public static int MaxRolls => 30;

        public static bool IsStartingLeft
        {
            get => _current._isStartingLeft;
            set => _current._isStartingLeft = value;
        }

        public static bool ShowHelpWindowAtStart {
            get => _current._showHelpWindowAtStart;
            set => _current._showHelpWindowAtStart = value;
        }

        public static bool SavePrintBlackWhite {
            get => _current._savePrintBlackWhite;
            set => _current._savePrintBlackWhite = value;
        }

        // readonly setting
        public static int MaxOrders => 10;

        // readonly setting
        public static int MinRollWidthForDisplayingTextHorizontally => 60;

        public static void Set(int maxNumberOfKnifes, double minKnifeDistance, double machineCenter,
            int maxRolls, double defaultRollWidth, bool isStartingLeft, bool showHelpWindowAtStart, bool savePrintBlackWhite)
        {
            _current = new ApplicationSetting(maxNumberOfKnifes, minKnifeDistance, machineCenter, maxRolls, defaultRollWidth, isStartingLeft, showHelpWindowAtStart, savePrintBlackWhite);
        }

        public static void Set(ApplicationSetting settings)
        {
            _current = new ApplicationSetting(settings._maxNumberOfKnifes, settings._minKnifeDistance, settings._machineCenter, settings._maxRolls, settings._defaultRollWidth, settings._isStartingLeft, settings._showHelpWindowAtStart, settings._savePrintBlackWhite);
        }

        public static ApplicationSetting Get()
        {
            return _current;
        }
    }
}