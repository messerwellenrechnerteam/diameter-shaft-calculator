﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.Properties;
using DiameterShaftCalculator.Utils;

namespace DiameterShaftCalculator.VM
{
    public enum Side { Links, Rechts}

    public class SettingsVm : INotifyPropertyChanged
    {
        private readonly SettingsWindow _settingsWindow;
        private readonly ApplicationVm _applicationVm;

        private ICommand _cancelCommand;

        private ICommand _saveCommand;

        public SettingsVm(SettingsWindow settingsWindow, ApplicationVm applicationVm)
        {
            _settingsWindow = settingsWindow;
            _applicationVm = applicationVm;
            CurrentSide = ApplicationSetting.IsStartingLeft ? Side.Links : Side.Rechts;
        }

        public int MaxNumberOfKnifes
        {
            get => ApplicationSetting.MaxNumberOfKnifes;
            set
            {
                if (ApplicationSetting.MaxNumberOfKnifes == value) return;
                ApplicationSetting.MaxNumberOfKnifes = value;
                OnPropertyChanged();
            }
        }

        public double MinKnifeDistance
        {
            get => ApplicationSetting.MinKnifeDistance;
            set
            {
                if (Math.Abs(ApplicationSetting.MinKnifeDistance - value) < 0.001) return;
                ApplicationSetting.MinKnifeDistance = Utility.RoundUpToPointFive(value);
                OnPropertyChanged();
            }
        }

        public double MachineCenter
        {
            get => ApplicationSetting.MachineCenter;
            set
            {
                if (Math.Abs(ApplicationSetting.MachineCenter - value) < 0.001) return;
                ApplicationSetting.MachineCenter = value;
                OnPropertyChanged();
            }
        }

        public double DefaultRollWidth
        {
            get => ApplicationSetting.DefaultRollWidth;
            set
            {
                if (Math.Abs(ApplicationSetting.DefaultRollWidth - value) < 0.001) return;
                ApplicationSetting.DefaultRollWidth = Utility.RoundUpToPointFive(value);
                OnPropertyChanged();
            }
        }

        public int MaxRolls => ApplicationSetting.MaxRolls;

        public bool ShowHelpWindowAtStart {
            get => ApplicationSetting.ShowHelpWindowAtStart;
            set {
                if (ApplicationSetting.ShowHelpWindowAtStart == value) return;
                ApplicationSetting.ShowHelpWindowAtStart = value;
                OnPropertyChanged();
            }
        }

        public bool SavePrintBlackWhite {
            get => ApplicationSetting.SavePrintBlackWhite;
            set {
                if (ApplicationSetting.SavePrintBlackWhite == value) return;
                ApplicationSetting.SavePrintBlackWhite = value;
                OnPropertyChanged();
            }   
        }

        public int MaxOrders => ApplicationSetting.MaxOrders;

        private Side _currentSide;
        public Side CurrentSide {
            get => _currentSide;
            set {
                if (_currentSide == value) return;
                _currentSide = value;

                ApplicationSetting.IsStartingLeft = value == Side.Links;

                OnPropertyChanged();
            }
        }

        public ICommand CancelCommand => _cancelCommand ?? (_cancelCommand = new RelayCommand(_ =>
        {
            MaxNumberOfKnifes = _settingsWindow.OldMaxNumberOfKnifes;
            MinKnifeDistance = _settingsWindow.OldMinKnifeDistance;
            MachineCenter = _settingsWindow.OldMachineCenter;
            DefaultRollWidth = _settingsWindow.OldDefaultRollWidth;
            CurrentSide = _settingsWindow.OldIsStartingLeft ? Side.Links : Side.Rechts;
            ShowHelpWindowAtStart = _settingsWindow.OldShowHelpWindowAtStart;
            SavePrintBlackWhite = _settingsWindow.OldSavePrintBlackWhite;
            _settingsWindow.Hide();
        }));

        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new RelayCommand(_ =>
        {
            new SettingsSerializer().Write(ApplicationSetting.Get(), ApplicationSetting.SettingsFilePath);
            _applicationVm.UpdateKnifes();
            _applicationVm.SettingsChanged();
            _settingsWindow.Hide();
            if (!(Math.Abs(MachineCenter - _settingsWindow.OldMachineCenter) > 0.0)) return;
            _applicationVm.MachineCenterChanged();
            // reset history so there are no problems if settings are changed
            _applicationVm.History.Clear();
            _applicationVm.Future.Clear();
            //MessageBox.Show("Um sicherzugehen, dass alle geänderten Einstellungen wirksam werden sollte das Programm neugestartet werden! Probleme können vor allem auftreten wenn bereits Aufträge gezeichnet wurden und der Wert der Maschinenmitte geändert wurde.", "Hinweis", MessageBoxButton.OK, MessageBoxImage.Information);
        }));

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class SampleSettingsVm
    {
        public ICommand CancelCommand => new RelayCommand(o => { });
        public ICommand SaveCommand => new RelayCommand(o => { });
        public int MaxNumberOfKnifes { get; set; } = 0;

        public double MinKnifeDistance { get; set; } = 0.0;

        public double MachineCenter { get; set; } = 0.0;

        public int MaxRolls { get; set; } = 0;

        public double DefaultRollWidth { get; set; } = 100.0;

        public Side CurrentSide { get; set; } = Side.Links;

        public bool ShowHelpWindowAtStart { get; set; }

        public int MaxOrders => ApplicationSetting.MaxOrders;

        public bool SavePrintBlackWhite { get; set; }
    }
}