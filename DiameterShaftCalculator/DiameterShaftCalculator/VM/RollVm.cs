﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DiameterShaftCalculator.Annotations;
using DiameterShaftCalculator.Utils;

namespace DiameterShaftCalculator.VM
{
    public class RollVm : INotifyPropertyChanged{
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public RollVm(double width, KnifeVm leftKnife, KnifeVm rightKnife, OrderVm orderVm){
            _width = width;
            _leftKnife = leftKnife;
            _rightKnife = rightKnife;
            OrderVm = orderVm;
        }

        public OrderVm OrderVm { get; }
        public RollVm ThisRollVm => this;

        public ColorInfo Color => OrderVm.Color;

        private KnifeVm _leftKnife;
        public KnifeVm LeftKnife {
            get => _leftKnife;
            set {
                if (Equals(_leftKnife, value)) return;
                _leftKnife = value;
                OnPropertyChanged();
            }
        }

        private KnifeVm _rightKnife;
        public KnifeVm RightKnife {
            get => _rightKnife;
            set {
                if (Equals(_rightKnife, value)) return;
                _rightKnife = value;
                OnPropertyChanged();
            }
        }

        private double _width;
        public double Width
        {
            get => _width;
            set
            {
                if (Math.Abs(_width - value) < 0.0001) return;
                _width = Utility.RoundUpToPointFive(value);
                OnPropertyChanged();

                OrderVm.InvokeRedraw();
            }
        }


        public double DrawPosition => _leftKnife.RealPosition + _width / 2;

        public override string ToString()
        {
            return _width+"";
        }

        public void KnifesChanged()
        {
            OnPropertyChanged(nameof(LeftKnife));
            OnPropertyChanged(nameof(RightKnife));
            OnPropertyChanged(nameof(ThisRollVm));
            LeftKnife.KnifeChanged();
            RightKnife.KnifeChanged();
        }

        public void OrderChanged()
        {
            OnPropertyChanged(nameof(OrderVm));
            ColorChanged();
        }

        public void ColorChanged() {
            OnPropertyChanged(nameof(Color));
            LeftKnife.ColorChanged();
            RightKnife.ColorChanged();

        }
    }
}
