﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using System.Windows.Input;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.HelpPages;
using DiameterShaftCalculator.Properties;
using DiameterShaftCalculator.Utils;

namespace DiameterShaftCalculator.VM
{
    public class HelpVm : INotifyPropertyChanged
    {
        private ICommand _backwardCommand;

        private Page _currentPage;

        private ICommand _forwardCommand;

        private int _idx;

        private ObservableCollection<Page> _pages;

        public HelpVm()
        {
            _pages = new ObservableCollection<Page>();

            // get all Classes which implements IHelpPage
            var type = typeof(IHelpPage);
            var subclasses = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p));

            foreach (var subclass in subclasses)
                if (subclass != type)
                    _pages.Add((Page) Activator.CreateInstance(subclass));

            CurrentPage = _pages.FirstOrDefault();
        }

        public ObservableCollection<Page> Pages
        {
            get => _pages;
            set
            {
                if (_pages == value) return;
                _pages = value;
                OnPropertyChanged();
            }
        }

        public Page CurrentPage
        {
            get => _currentPage;
            set
            {
                if (_currentPage == value) return;
                _currentPage = value;
                OnPropertyChanged();
            }
        }

        public bool ShowHelpWindowAtStart {
            get => ApplicationSetting.ShowHelpWindowAtStart;
            set {
                if (ApplicationSetting.ShowHelpWindowAtStart == value) return;
                ApplicationSetting.ShowHelpWindowAtStart = value;
                OnPropertyChanged();
                new SettingsSerializer().Write(ApplicationSetting.Get(), ApplicationSetting.SettingsFilePath);
            }
        }

        public ICommand ForwardCommand => _forwardCommand ?? (_forwardCommand = new RelayCommand(_ =>
        {
            _idx++;
            if (_idx >= _pages.Count) _idx = 0;
            CurrentPage = _pages[_idx];
        }));

        public ICommand BackwardCommand => _backwardCommand ?? (_backwardCommand = new RelayCommand(_ =>
        {
            _idx--;
            if (_idx < 0) _idx = _pages.Count - 1;
            CurrentPage = _pages[_idx];
        }));

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}