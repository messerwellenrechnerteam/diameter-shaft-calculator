﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Input;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.Properties;
using DiameterShaftCalculator.Utils;

namespace DiameterShaftCalculator.VM
{
    public class OrderVm : INotifyPropertyChanged, ICloneable
    {
        private double _centerOffset;
        private CutTypeEnum _selectedCutType;
        private ObservableCollection<KnifeVm> _knifes;
        private ObservableCollection<RollVm> _rollWidths;
        private double _singleRollWidth = ApplicationSetting.DefaultRollWidth;
        private string _orderId;
        private long _orderNumber;
        private bool _variableWidths;
        private Visibility _variableWidthsVisibility = Visibility.Collapsed;
        private Visibility _singleWidthVisibility = Visibility.Visible;
        private int _numberOfRolls;

        private NewOrderWindow _orderWindow;
        private bool _isNewOrder = true;
        private OrderVm OldOrder { get; set; }

        public OrderVm CreateCopy(bool saveInOldOrder = true)
        {
            var old = (OrderVm) MemberwiseClone();
            old.RollWidths = CopyRollWidths(this);
            old.Knifes = CopyKnifes(this);
            Color = old.Color;
            old.Color = new ColorInfo(old.Color.ColorName, old.Color.Color);

            if (saveInOldOrder) OldOrder = old;

            return old;
        }

        private ObservableCollection<KnifeVm> CopyKnifes(OrderVm order) {
            var knifes = new ObservableCollection<KnifeVm>();
            foreach (var oldknife in Knifes)
            {
                knifes.Add(new KnifeVm(oldknife.Knife, order));
            }

            return knifes;
        }

        private ObservableCollection<RollVm> CopyRollWidths(OrderVm order)
        {
            var rolls = new ObservableCollection<RollVm>();
            foreach (var oldroll in RollWidths)
            {
                rolls.Add(new RollVm(oldroll.Width, oldroll.LeftKnife, oldroll.RightKnife, order));
            }

            return rolls;
        }

        public OrderVm(ApplicationVm applicationVm, NewOrderWindow orderWindow) : this(applicationVm, 0, "", CutTypeEnum.Beidseitig, false){
            _orderWindow = orderWindow;
        }

        public OrderVm(ApplicationVm applicationVm, int orderNumber, string orderId, CutTypeEnum cutType,
            bool variableWidths) {
            ApplicationVm = applicationVm;
            _orderNumber = orderNumber;
            if (orderNumber == 0)
            {
                _orderNumber = ApplicationVm.Orders.Count + 1;
            }
            _orderId = orderId;
            SelectedCutType = cutType;
            _variableWidths = variableWidths;
            _knifes = new ObservableCollection<KnifeVm>();
            NumberOfRolls = 1;
            SingleRollWidth = ApplicationSetting.DefaultRollWidth;
            _color = Colormanager.NextColor();
        }

        #region MoveKeys

        public string MoveLeftKey => ApplicationVm.MoveLeftKey;
        public string MoveRightKey => ApplicationVm.MoveRightKey;

        public void MoveKeyChanged(){
            OnPropertyChanged(nameof(MoveLeftKey));
            OnPropertyChanged(nameof(MoveRightKey));
        }

        #endregion

        #region Color
        private ColorInfo _color;
        public ColorInfo Color {
            get => _color;
            set {
                if (_color == value) return;
                _color = value;
                OnPropertyChanged();
            }
        }
        public IList<ColorInfo> Colors => Colormanager.GetColors();

        #endregion

        #region div_properties
        public ApplicationVm ApplicationVm { get; }

        public bool IsNewOrder
        {
            get => _isNewOrder;
            set{
                if (_isNewOrder == value) return;
                _isNewOrder = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsNotNewOrder));
            }
        }

        public bool IsNotNewOrder => !IsNewOrder;

        public OrderVm ThisOrder => this;

        public NewOrderWindow OrderWindow
        {
            get => _orderWindow;
            set
            {
                if (Equals(_orderWindow, value)) return;
                _orderWindow = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region order_properties
        public long OrderNumber
        {
            get => _orderNumber;
            set
            {
                if (_orderNumber == value) return;
                _orderNumber = value;
                OnPropertyChanged();
            }
        }

        public string OrderId
        {
            get => _orderId;
            set
            {
                if (_orderId == value) return;
                _orderId = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<KnifeVm> Knifes
        {
            get => _knifes;
            set
            {
                if (_knifes == value) return;
                _knifes = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<RollVm> RollWidths
        {
            get => _rollWidths;
            set {
                if (_rollWidths == value) return;
                _rollWidths = value;
                OnPropertyChanged();
            }
        }

        public double SingleRollWidth {
            get => _singleRollWidth;
            set {
                if (Math.Abs(_singleRollWidth - value) < 0.0001) return;
                _singleRollWidth = value < ApplicationSetting.MinKnifeDistance ? ApplicationSetting.DefaultRollWidth : value;
                _singleRollWidth = Utility.RoundUpToPointFive(_singleRollWidth);
                OnPropertyChanged();
                InititializeRollWidths();
                InvokeRedraw();
            }
        }

        public CutTypeEnum SelectedCutType
        {
            get => _selectedCutType;
            set {
                if (_selectedCutType == value) return;
                _selectedCutType = value;
                OnPropertyChanged();
                if (IsNewOrder) return;

                var left = SelectedCutType == CutTypeEnum.Mitteschneider ||
                           SelectedCutType == CutTypeEnum.Rechts;
                var right = SelectedCutType == CutTypeEnum.Mitteschneider ||
                           SelectedCutType == CutTypeEnum.Links;

                if (Knifes?.Count > 0) {
                    Knifes.First().IsVirtual = left;
                    Knifes.First().ColorChanged();
                    Knifes.Last().IsVirtual = right;
                    Knifes.Last().ColorChanged();
                }

                if (RollWidths?.Count > 0) {
                    RollWidths.First().LeftKnife.IsVirtual = left;
                    RollWidths.First().ColorChanged();
                    RollWidths.Last().RightKnife.IsVirtual = right;
                    RollWidths.Last().ColorChanged();
                }
            }
        }

        public bool VariableWidths
        {
            get => _variableWidths;
            set
            {
                if (_variableWidths == value) return;
                _variableWidths = value;
                VariableWidthsVisibility = _variableWidths ? Visibility.Visible : Visibility.Collapsed;
                SingleWidthVisibility = _variableWidths ? Visibility.Collapsed : Visibility.Visible;
                OnPropertyChanged();
                InvokeRedraw();
            }
        }

        public Visibility VariableWidthsVisibility
        {
            get => _variableWidthsVisibility;
            set
            {
                if (_variableWidthsVisibility == value) return;
                _variableWidthsVisibility = value;
                OnPropertyChanged();
            }
        }

        public Visibility SingleWidthVisibility {
            get => _singleWidthVisibility;
            set {
                if (_singleWidthVisibility == value) return;
                _singleWidthVisibility = value;
                OnPropertyChanged();
            }
        } 

        public double CenterOffset
        {
            get => _centerOffset;
            set
            {
                if (Math.Abs(_centerOffset - value) < 0.01) return;
                _centerOffset = value;
                OnPropertyChanged();
            }
        }

        public int NumberOfRolls
        {
            get => _numberOfRolls;
            set{
                if (_numberOfRolls == value) return;
                _numberOfRolls = value < 1 ? 1 : value;
                _numberOfRolls = _numberOfRolls > ApplicationSetting.MaxRolls ? ApplicationSetting.MaxRolls : _numberOfRolls;
                OnPropertyChanged();
                InititializeRollWidths();
                InvokeRedraw();
            }
        }

        private void InititializeRollWidths(double? width = null) {
            if(RollWidths == null) RollWidths = new ObservableCollection<RollVm>();
            else RollWidths.Clear();
            var rollwidth = width ?? SingleRollWidth;
            for (var i = 0; i < _numberOfRolls; i++)
            {
                RollWidths.Add(new RollVm(rollwidth, null, null, this));
            }
        }

        public double LeftPosition => _knifes.First().Knife.RealPosition;
        public double RightPosition => _knifes.Last().Knife.RealPosition;

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #region methods

        private readonly object _locker = new object();
        public void MoveKnifes(double offset, bool saveInHistory = true) {
            if (_knifes.Count == 0) return;
            if (Math.Abs(offset) < 0.1) return;
            if (offset < 0 && _knifes.First().RealPosition <= 0.0) return;
            if (offset > 0 && _knifes.Last().RealPosition >= ApplicationSetting.MachineWidth) return;

            lock (_locker) { 
                _centerOffset += offset;
                foreach (var knifeVm in _knifes) knifeVm.Move(offset);
                OnPropertyChanged(nameof(RollWidths));
                OnPropertyChanged(nameof(LeftPosition));
                OnPropertyChanged(nameof(RightPosition));
                foreach (var rollWidth in RollWidths)
                {
                    rollWidth.KnifesChanged();
                }

                ApplicationVm.UpdateKnifes();
                if (!saveInHistory) return;
                ApplicationVm.History.Push(new Tuple<Action, Action>(() => MoveKnifes(offset * -1, false), () => MoveKnifes(offset * 1, false)));
                ApplicationVm.Future.Clear();
            }
        }

        public void UpdateRolls()
        {
            OnPropertyChanged(nameof(ThisOrder));
            foreach (var rollWidth in RollWidths)
            {
                rollWidth.KnifesChanged();
                rollWidth.OrderChanged();
            }
        }

        public void Center(bool saveInHistory = true)
        {
            MoveKnifes(_centerOffset * -1.0, saveInHistory);
            _centerOffset = 0;
        }

        public void Delete()
        {
            ApplicationVm.DeleteOrder(this);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("OrderNumber: " + OrderNumber);
            sb.Append(" OrderId: " + OrderId);
            sb.Append(" CutType: " + SelectedCutType);
            sb.Append(" VariabelWidths: " + VariableWidths);
            var knifes = "\nKnife arrangment: ";
            for (var i = 0; i < Knifes.Count; i++){
                if (Knifes[i].IsVirtual) knifes += ".";
                else knifes += "|";
                if(i < Knifes.Count -1) knifes += RollWidths[i];
            }
            sb.Append(knifes);
            return sb.ToString();
        }

        public object Clone(){
            return CreateCopy(false);
        }

        public double TotalWidth => RollWidths.Sum(vm => vm.Width);

        public void InvokeRedraw() {
            if (IsNewOrder) return;
            CalculateKnifes();
            OnPropertyChanged(nameof(Knifes));
            OnPropertyChanged(nameof(LeftPosition));
            OnPropertyChanged(nameof(RightPosition));
        }

        public void CalculateKnifes(){
            // clear knifes
            Knifes.Clear();
            // add first knife

            var lastWidth = ApplicationSetting.MachineCenter - (TotalWidth / 2) + CenterOffset;

            Knifes.Add(new KnifeVm(new Knife(lastWidth, SelectedCutType == CutTypeEnum.Mitteschneider || SelectedCutType == CutTypeEnum.Rechts), this));
            // add all other knifes
            foreach (var roll in RollWidths)
            {
                lastWidth += roll.Width;
                Knifes.Add(new KnifeVm(new Knife(lastWidth), this));
            }
            // edit last knife virtuality
            Knifes.Last().IsVirtual = SelectedCutType == CutTypeEnum.Mitteschneider || SelectedCutType == CutTypeEnum.Links;

            // add knifes to rolls
            for (var i = 0; i < RollWidths.Count; i++)
            {
                RollWidths[i].LeftKnife = Knifes[i];
                RollWidths[i].RightKnife = Knifes[i + 1];
            }

            // errorprevention if width is not .0 or .5
            if (TotalWidth * 10 % 5 != 0.0)
            {
                var offset = (TotalWidth * 10 % 5) / 10;
                MoveKnifes(offset, false);
            }

            if (Knifes.Count > 0 && Knifes[0].RealPosition * 10 % 5 != 0.0)
            {
                var offset = (Knifes[0].RealPosition * 10 % 5) / 10;
                MoveKnifes(offset, false);
            }

            if (Knifes.Last().RealPosition > ApplicationSetting.MachineWidth) {
                MoveKnifes(ApplicationSetting.MachineWidth - Knifes.Last().RealPosition, false);
            }
            if (Knifes.First().RealPosition < 0)
            {
                MoveKnifes(Knifes.First().RealPosition * -1, false);
            }
            OnPropertyChanged(nameof(RollWidths));
            OnPropertyChanged(nameof(Knifes));
            OnPropertyChanged(nameof(LeftPosition));
            OnPropertyChanged(nameof(RightPosition));
        }

        #endregion

        #region Commands

        private ICommand _deleteCommand;

        public ICommand DeleteCommand => _deleteCommand ?? (_deleteCommand = new RelayCommand(_ =>
        {
            Delete();
        }));

        private ICommand _moveLeftCommand;

        public ICommand MoveLeftCommand => _moveLeftCommand ?? (_moveLeftCommand = new RelayCommand(_ =>
        {
            ApplicationVm.CurrentOrder = this;
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                MoveKnifes(-5);
            }
            else
            {
                MoveKnifes(-0.5);
            }
        }));

        private ICommand _moveRightCommand;

        public ICommand MoveRightCommand => _moveRightCommand ?? (_moveRightCommand = new RelayCommand(_ =>
        {
            ApplicationVm.CurrentOrder = this;
            if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
            {
                MoveKnifes(5);
            }
            else
            {
                MoveKnifes(0.5);
            }
        }));

        private ICommand _moveFarLeftCommand;
        public ICommand MoveFarLeftCommand => _moveFarLeftCommand ?? (_moveFarLeftCommand = new RelayCommand(_ => {
            ApplicationVm.CurrentOrder = this;
            MoveKnifes(-5);
        }));

        private ICommand _moveFarRightCommand;
        public ICommand MoveFarRightCommand => _moveFarRightCommand ?? (_moveFarRightCommand = new RelayCommand(_ => {
            ApplicationVm.CurrentOrder = this;
            MoveKnifes(5);
        }));

        private ICommand _centerCommand;

        public ICommand CenterCommand => _centerCommand ?? (_centerCommand = new RelayCommand(_ =>
        {
            ApplicationVm.CurrentOrder = this;
            Center();
        }));

        private ICommand _editCommand;

        public ICommand EditCommand => _editCommand ?? (_editCommand = new RelayCommand(_ =>{
            ApplicationVm.CurrentOrder = this;
            new NewOrderWindow(this).Show();
        }));

        private ICommand _closeCommand;
        public ICommand CloseCommand => _closeCommand?? (_closeCommand = new RelayCommand(_ => {
            if (!_isNewOrder){
                ResetToOldOrder();

                ApplicationVm.UpdateKnifes();
                OnPropertyChanged(nameof(LeftPosition));
                OnPropertyChanged(nameof(RightPosition));
                OnPropertyChanged(nameof(RollWidths));
            }
            _orderWindow.Hide();
        }));

        private void ResetToOldOrder(OrderVm old = null) {
            if (old == null && OldOrder == null) return;

            if (old == null) old = OldOrder;

            CenterOffset = old._centerOffset;
            SelectedCutType = old._selectedCutType;
            Knifes = old._knifes;
            RollWidths = old._rollWidths;
            SingleRollWidth = old._singleRollWidth;
            OrderId = old._orderId;
            OrderNumber = old._orderNumber;
            VariableWidths = old._variableWidths;
            VariableWidthsVisibility = old._variableWidthsVisibility;
            SingleWidthVisibility = old._singleWidthVisibility;
            NumberOfRolls = old.NumberOfRolls;
            Color = old.Color;
        }

        private ICommand _saveCommand;
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new RelayCommand(_ => {
            // ---------------------------------
            // Setting checks
            // ---------------------------------
            // check number of rolls
            if (_numberOfRolls <= 0){
                MessageBox.Show("Wert für Nutzenanzahl nicht erlaubt!", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (_numberOfRolls == 1 && SelectedCutType == CutTypeEnum.Mitteschneider){
                MessageBox.Show("Mitteschneider bei einzelner Rolle nicht möglich!", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // check width of single roll
            if (!VariableWidths  && SingleRollWidth < ApplicationSetting.MinKnifeDistance) {
                MessageBox.Show("Nutzenbreite zu gering!", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // check width of different sized rolls
            if (VariableWidths) {
                for (var i = 0; i < RollWidths.Count; i++){
                    RollWidths[i].Width = Utility.RoundUpToPointFive(RollWidths[i].Width);
                    if (!(RollWidths[i].Width < ApplicationSetting.MinKnifeDistance)) continue;
                    MessageBox.Show("Nutzenbreite von Nutzen "+(i+1)+" zu gering!", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            // check total order with
            var totalWidth = RollWidths.Sum(vm => vm.Width);
            if (totalWidth > ApplicationSetting.MachineCenter * 2){
                MessageBox.Show("Auftrag zu breit", "Fehler", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            // if order is a new order add it to collection
            if (_isNewOrder)
            {
                // Calculate Knifes
                CalculateKnifes();
                ApplicationVm.AddOrder(this);
                IsNewOrder = false;
            }
            else
            {
                OnPropertyChanged(nameof(LeftPosition));
                OnPropertyChanged(nameof(RightPosition));
                OnPropertyChanged(nameof(RollWidths));
                ApplicationVm.UpdateKnifes();

                if (Knifes.Last().RealPosition > ApplicationSetting.MachineWidth)
                {
                    MoveKnifes(ApplicationSetting.MachineWidth - Knifes.Last().RealPosition, false);
                }
                if (Knifes.First().RealPosition < 0)
                {
                    MoveKnifes(Knifes.First().RealPosition * -1, false);
                }

                var copy = CreateCopy(false);
                ApplicationVm.History.Push(new Tuple<Action, Action>(() => { ResetToOldOrder(copy.OldOrder);}, () => { ResetToOldOrder(copy); }));
                ApplicationVm.Future.Clear();
            }

            _orderWindow.Hide();
        }));
        #endregion
    }

    public class SampleOrderVm{
        public SampleOrderVm(){
            Knifes = new ObservableCollection<KnifeVm>();
            for (var i = 0; i < 10; i++){
                Knifes.Add(new KnifeVm(new Knife(100, i == 0 || i == 9), null));
            }

            RollWidths = new ObservableCollection<RollVm>();
            for (var i = 0; i < NumberOfRolls; i++) {
                RollWidths.Add(new RollVm(100, null, null, null));
            }

            Color = Colormanager.NextColor();
        }

        public int OrderNumber { get; set; } = 10;

        public string OrderId { get; set; } = "Id";

        public ObservableCollection<KnifeVm> Knifes { get; set; }

        public CutTypeEnum SelectedCutType { get; set; } = CutTypeEnum.Beidseitig;

        public bool VariableWidths { get; set; } = true;
        public Visibility VariableWidthsVisibility { get; set; } = Visibility.Visible;
        public Visibility SingleWidthVisibility { get; set; } = Visibility.Collapsed;

        public double CenterOffset { get; set; } = 0.0;

        public double LeftPosition { get; set; } = 100.0;
        public double RightPosition { get; set; } = 300.0;

        public int NumberOfRolls { get; set; } = 5;

        public ObservableCollection<RollVm> RollWidths { get; set; }
        public double SingleRollWidth { get; set; } = 100.0;

        public ColorInfo Color { get; set; }
        public IList<ColorInfo> Colors => Colormanager.GetColors();

        public bool IsNewOrder => true;

        public bool IsNotNewOrder => !IsNewOrder;

        #region Commands
        public ICommand DeleteCommand => new RelayCommand(o => { });
        public ICommand MoveLeftCommand => new RelayCommand(o => { });
        public ICommand MoveRightCommand => new RelayCommand(o => { });
        public ICommand CenterCommand => new RelayCommand(o => { });
        public ICommand EditCommand => new RelayCommand(o => { });
        public ICommand CloseCommand => new RelayCommand(o => { });
        public ICommand SaveCommand => new RelayCommand(o => { });

        #endregion
    }
}