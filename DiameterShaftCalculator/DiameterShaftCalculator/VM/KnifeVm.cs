﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.Properties;
using DiameterShaftCalculator.Utils;

namespace DiameterShaftCalculator.VM
{
    public class KnifeVm : INotifyPropertyChanged, IEquatable<KnifeVm>
    {
        private Knife _knife;

        public KnifeVm()
        {
        }

        public KnifeVm(Knife knife, OrderVm orderVm)
        {
            _knife = knife;
            OrderVm = orderVm;
        }

        public OrderVm OrderVm { get; }

        public ColorInfo Color => OrderVm.Color;

        public bool IsVirtual
        {
            get => _knife.IsVirtual;
            set
            {
                if (_knife.IsVirtual == value) return;
                _knife.IsVirtual = value;
                OnPropertyChanged();
            }
        }

        public bool IsNotVirtual => !IsVirtual;

        public double RealPosition
        {
            get => _knife.RealPosition;
            set
            {
                if (Math.Abs(_knife.RealPosition - value) < 0.0001) return;
                _knife.RealPosition = value;
                OnPropertyChanged();
            }
        }

        public Knife Knife
        {
            get => _knife;
            set
            {
                if (_knife == value) return;
                _knife = value;
                OnPropertyChanged();
            }
        }

        public void Move(double offset)
        {
            Knife.Move(offset);
            OnPropertyChanged(nameof(Knife));
            OnPropertyChanged(nameof(RealPosition));

        }

        public void KnifeChanged() {
            OnPropertyChanged(nameof(IsVirtual));
            OnPropertyChanged(nameof(IsNotVirtual));
            OnPropertyChanged(nameof(RealPosition));

        }

        public void ColorChanged()
        {
            OnPropertyChanged(nameof(Color));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool Equals(KnifeVm other){
            if (ReferenceEquals(null, other)) return false;
            return ReferenceEquals(this, other) || Equals(_knife, other._knife);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((KnifeVm) obj);
        }

        public override int GetHashCode()
        {
            return (_knife != null ? _knife.GetHashCode() : 0);
        }
    }
}