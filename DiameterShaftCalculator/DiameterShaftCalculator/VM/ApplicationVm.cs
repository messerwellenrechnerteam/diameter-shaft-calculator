﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Printing;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DiameterShaftCalculator.Domain;
using DiameterShaftCalculator.Properties;
using DiameterShaftCalculator.Utils;
using Parago.Windows;
using Timer = System.Timers.Timer;

namespace DiameterShaftCalculator.VM
{
    public class ApplicationVm : INotifyPropertyChanged{
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private readonly Canvas _canvas;
        private readonly MainWindow _window;

        public ApplicationVm(Canvas canvas, MainWindow window){
            _canvas = canvas;
            _window = window;
            History = new ObservableStack<Tuple<Action, Action>>();
            Future = new ObservableStack<Tuple<Action, Action>>();
            Orders = new ObservableCollection<OrderVm>();// { CreateRandomOrderVm(), CreateRandomOrderVm() };
            Knifes = new ObservableMulitSet<KnifeVm>(10);
            KnifeDistances = new ObservableCollection<string>();
            UpdateKnifes();
        }

        private string _moveLeftKey = "<";
        public string MoveLeftKey{
            get => _moveLeftKey;
            set
            {
                if (value == _moveLeftKey) return;
                _moveLeftKey = value;
                MoveKeyChanged();
                OnPropertyChanged();
            }
        }
        private string _moveRightKey = ">";
        public string MoveRightKey {
            get => _moveRightKey;
            set
            {
                if (value == _moveRightKey) return;
                _moveRightKey = value;
                MoveKeyChanged();
                OnPropertyChanged();
            }
        }

        private void MoveKeyChanged(){
            foreach (var order in Orders)
            {
                order.MoveKeyChanged();
            }
        }

        public int MaxOrders => ApplicationSetting.MaxOrders;

        public double MachineCenter => ApplicationSetting.MachineCenter;

        public void MachineCenterChanged() {
            OnPropertyChanged(nameof(MachineCenter));
            foreach (var order in Orders) {
                order.CalculateKnifes();
             }
        }
        public double ZeroValue => 0.0;

        private ColorInfo _globalColor;

        public ColorInfo GlobalColor {
            get => _globalColor;
            set {
                if (Equals(_globalColor, value)) return;
                _globalColor = value;
                OnPropertyChanged();
            }
        }

        #region OperationHistory

        private ObservableStack<Tuple<Action, Action>> _history;
        public ObservableStack<Tuple<Action, Action>> History
        {
            get => _history;
            set
            {
                if (_history == value) return;
                _history = value;
                OnPropertyChanged();
            }
        }

        private ObservableStack<Tuple<Action, Action>> _future;
        public ObservableStack<Tuple<Action, Action>> Future {
            get => _future;
            set {
                if (_future == value) return;
                _future = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region RandomOrders
        private readonly Random _rand = new Random();
        private OrderVm CreateRandomOrderVm(){
            var result = new OrderVm(this, null);
            result.SelectedCutType = CutTypeEnum.Mitteschneider;
            result.OrderId = "Order" + _rand.Next(0, 100);
            var val = _rand.NextDouble() * 100;
            result.Knifes.Add(new KnifeVm(new Knife(val), result));
            var maxI = _rand.Next(1, 10);
            result.NumberOfRolls = maxI;
            for (var i = 0; i < maxI; i++){
                var oldval = val;
                val += _rand.Next(50, 100);
                var knife = new KnifeVm(new Knife(val), result);
                result.Knifes.Add(knife);
                if (i < maxI -1){
                    result.RollWidths.Add(new RollVm(val - oldval, knife, knife, result));
                }
            }
            result.VariableWidths = true;
            return result;
        }
#endregion

        #region orders
        private OrderVm _currentOrder;

        public OrderVm CurrentOrder
        {
            get => _currentOrder;
            set
            {
                if (_currentOrder == value) return;
                _currentOrder = value;
                OnPropertyChanged();
            }
        }

        private ObservableCollection<OrderVm> _orders;
        public ObservableCollection<OrderVm> Orders {
            get => _orders;
            set{
                if (_orders == value) return;
                _orders = value;
                OnPropertyChanged();
            }
        }

        public void AddOrder(OrderVm order, bool saveInHistory = true) {
            Orders.Add(order);
            if (saveInHistory) { 
                History.Push(new Tuple<Action, Action>(() => { DeleteOrder(order, false); }, () => { AddOrder(order, false); }));
                Future.Clear();
            }
            UpdateKnifes();
        }

        public void DeleteOrder(OrderVm order, bool saveInHistory = true) {
            Orders.Remove(order);
            if (saveInHistory) { 
                History.Push(new Tuple<Action, Action>( ()=> { AddOrder(order, false); }, () => { DeleteOrder(order, false); }));
                Future.Clear();
            }
            UpdateKnifes();
        }


        private void CheckSettings()
        {
            if (Knifes.Count > ApplicationSetting.MaxNumberOfKnifes)
            {
                var result = MessageBox.Show("Zu viele Messer gezeichnet", "Hinweis", MessageBoxButton.OK, MessageBoxImage.Warning);

            }

            foreach (var Knife in KnifeDistances)
            {
                if (!double.TryParse(Knife, out double value)) continue;
                if (!(value < ApplicationSetting.MinKnifeDistance)) continue;
                var result = MessageBox.Show("Ein Messerabstand unterschreitet den Mindestabstand", "Hinweis", MessageBoxButton.OK, MessageBoxImage.Warning);
                break;
            }
        }

        #endregion

        #region Knifes

        private ObservableMulitSet<KnifeVm> _knifes;
        public ObservableMulitSet<KnifeVm> Knifes
        {
            get => _knifes;
            set
            {
                if (_knifes == value) return;
                _knifes = value;
                OnPropertyChanged();
            }
        }

        public void UpdateKnifes(){
            _knifes.Clear();
            foreach (var order in Orders){
                order.UpdateRolls();
                foreach (var knife in order.Knifes){
                    if(!knife.IsVirtual)
                      _knifes.Add(knife);
                }
            }
            OnPropertyChanged(nameof(Orders));

            UpdateKnifeDistances();
        }

        private void UpdateKnifeDistances()
        {
            var knifes = _knifes.Select(knife => knife.Key.RealPosition).ToList();
            knifes.Sort();
            var result = new ObservableCollection<string>();
            if (ApplicationSetting.IsStartingLeft)
            {


                for (var i = 0; i < _knifes.Count; i++)
                {
                    if (i == 0) result.Add("" + knifes[i]);
                    else result.Add("" + Math.Abs(knifes[i] - knifes[i - 1]));
                    result.Add("|");
                }
            }
            else
            {
                for (var i = knifes.Count - 1; i >= 0; i--)
                {
                    if (i == knifes.Count - 1) result.Add("" + Math.Abs(ApplicationSetting.MachineWidth - knifes[i]));
                    else result.Add("" + Math.Abs(knifes[i + 1] - knifes[i]));
                    result.Add("|");
                }
                result = new ObservableCollection<string>(result.Reverse());
            }

            KnifeDistances = result;
        }

        private ObservableCollection<string> _knifeDistances;
        public ObservableCollection<string> KnifeDistances {
            get => _knifeDistances;
            set {
                if (_knifeDistances == value) return;
                _knifeDistances = value;
                OnPropertyChanged();
            }
        }

        public bool IsStartingLeft => ApplicationSetting.IsStartingLeft;

        public void SettingsChanged(){
            OnPropertyChanged(nameof(IsStartingLeftText));
            UpdateKnifes();
        }
        public string IsStartingLeftText => ApplicationSetting.IsStartingLeft ? "Links" : "Rechts";

        #endregion

        #region Commands

        private ICommand _autoMoveCommand;

        public ICommand AutoMoveCommand => _autoMoveCommand ?? (_autoMoveCommand = new RelayCommand(_ =>
        {
            CurrentOrder = null;
            ProgressDialogResult result = ProgressDialog.Execute(_window, "Automatisch Ausrichten", (bw, we) =>
                {
                    var progess = 0;
                    if (!ProgressDialog.ReportWithCancellationCheck(bw, we, "Fortschritt {0} %...", progess))
                    {
                        foreach (var j in Orders) {
                            j.Center();
                        }

                        var knifes = new HashSet<KnifeVm>();
                        var minMoves = new List<double> { 0.0 };
                        var count = Orders.Count;
                        for (var i = 1; i < count; i++) {
                            updateKnifesList(knifes, Orders.GetRange(0, i + 1));
                            var movedLeft = 0.0;
                            while (containsToLowDistance(knifes)) {
                                Orders[i].MoveKnifes(-0.5, false);
                                updateKnifesList(knifes, Orders.GetRange(0, i + 1));
                            }
                            Orders[i].MoveKnifes(movedLeft * -1, false);
                            updateKnifesList(knifes, Orders.GetRange(0, i));

                            var movedRight = 0.0;
                            while (containsToLowDistance(knifes)) {
                                movedLeft += 0.5;
                                Orders[i].MoveKnifes(0.5);
                                updateKnifesList(knifes, Orders.GetRange(0, i + 1));
                            }
                            if (Math.Abs(movedLeft) < movedRight) {
                                Orders[i].MoveKnifes(movedLeft);
                                minMoves.Add(movedLeft);
                            } else {
                                minMoves.Add(movedRight);
                            }
                            progess = i / count * 100;
                        }

                        for (var j = 0; j < Orders.Count; j++) {
                            Orders[j].MoveKnifes(minMoves[j]);
                            Orders[j].UpdateRolls();
                        }
                    }

                }
            );

        }));



        private void updateKnifesList(ICollection<KnifeVm> knifes, IEnumerable<OrderVm> jobs)
        {
            knifes.Clear();
            foreach(var j in jobs) { 
                knifes.AddAll(j.Knifes.Where(vm => vm.IsNotVirtual));
            }
        }

        private bool containsToLowDistance(IEnumerable<KnifeVm> knifes){
            var query = knifes.OrderBy(k => k.RealPosition).ToList();
            for (var i = 1; i < query.Count(); i++)
            {
                if (query[i].RealPosition - query[i - 1].RealPosition < ApplicationSetting.MinKnifeDistance)
                {
                    return true;
                }
            }
            return false;
        }


        private ICommand _newOrderCommand;

        public ICommand NewOrderCommand => _newOrderCommand ?? (_newOrderCommand = new RelayCommand(_ =>
        {
            if (Orders.Count >= ApplicationSetting.MaxOrders){
                MessageBox.Show("Maximal "+ ApplicationSetting.MaxOrders+" Aufträge möglich.", "Hinweis", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            CurrentOrder = null;
            new NewOrderWindow(this).Show();
        }));

        private ICommand _settingsWindowCommand;

        public ICommand SettingsWindowCommand => _settingsWindowCommand ?? (_settingsWindowCommand = new RelayCommand(
                                                     _ =>
                                                     {
                                                         CurrentOrder = null;
                                                         SettingsWindow.ShowInstance(this);
                                                     }));

        private ICommand _centerAllCommand;

        public ICommand CenterAllCommand => _centerAllCommand ?? (_centerAllCommand = new RelayCommand(_ =>
        {
            foreach (var o in Orders) o.Center();
        }));

        private ICommand _deleteAllCommand;

        public ICommand DeleteAllCommand => _deleteAllCommand ?? (_deleteAllCommand = new RelayCommand(_ =>
        {
            var res = MessageBox.Show("Wollen Sie wirklich alle Aufträge zurücksetzen?", "Alles zurücksetzen?", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (res != MessageBoxResult.OK) return;
            Orders.Clear();
            History.Clear();
            Future.Clear();
            UpdateKnifes();
        }));

        private ICommand _showHelpCommand;

        public ICommand ShowHelpCommand => _showHelpCommand ??
                                           (_showHelpCommand = new RelayCommand(_ => { CurrentOrder = null; HelpWindow.ShowInstance(); }));

        private ICommand _showInformationCommand;

        public ICommand ShowInformationCommand => _showInformationCommand ??
                                                  (_showInformationCommand = new RelayCommand(_ =>
                                                  {
                                                      CurrentOrder = null;
                                                      ProductInformationWindow.ShowInstance();
                                                  }));

        private ICommand _previousCommand;

        public ICommand PreviousCommand => _previousCommand ?? (_previousCommand = new RelayCommand(_ =>
        {
            if (Orders.Count == 0) return;
            if (CurrentOrder == null)
            {
                CurrentOrder = Orders.Last();
            }
            else
            {
                var idx = Orders.IndexOf(CurrentOrder);
                idx = idx == Orders.Count - 1 ? 0 : idx + 1;
                CurrentOrder = Orders[idx];
            }
        }));

        private ICommand _nextCommand;

        public ICommand NextCommand => _nextCommand ?? (_nextCommand = new RelayCommand(_ =>
        {
            if (Orders.Count == 0) return;
            if (CurrentOrder == null)
            {
                CurrentOrder = Orders.First();
            }
            else
            {
                var idx = Orders.IndexOf(CurrentOrder);
                idx = idx == 0 ? Orders.Count() - 1 : idx - 1;
                CurrentOrder = Orders[idx];
            }
        }));

        private ICommand _moveLeftCommand;

        public ICommand MoveLeftCommand => _moveLeftCommand ?? (_moveLeftCommand = new RelayCommand(_ =>
        {
            if (CurrentOrder == null) return;
            CurrentOrder.MoveLeftCommand.Execute(null);
        }));

        private ICommand _moveRightCommand;

        public ICommand MoveRightCommand => _moveRightCommand ?? (_moveRightCommand = new RelayCommand(_ =>
        {
            if (CurrentOrder == null) return;
            CurrentOrder.MoveRightCommand.Execute(null);
        }));

        private ICommand _moveFarLeftCommand;
        public ICommand MoveFarLeftCommand => _moveFarLeftCommand ?? (_moveFarLeftCommand = new RelayCommand(_ => {
            if (CurrentOrder == null) return;
            CurrentOrder.MoveFarLeftCommand.Execute(null);
        }));

        private ICommand _moveFarRightCommand;
        public ICommand MoveFarRightCommand => _moveFarRightCommand ?? (_moveFarRightCommand = new RelayCommand(_ => {
            if (CurrentOrder == null) return;
            CurrentOrder.MoveFarRightCommand.Execute(null);
        }));

        private ICommand _centerCommand;
        public ICommand CenterCommand => _centerCommand ?? (_centerCommand = new RelayCommand(_ => {
            if (CurrentOrder == null) return;
            CurrentOrder.CenterCommand.Execute(null);
        }));

        private ICommand _editCommand;
        public ICommand EditCommand => _editCommand ?? (_editCommand = new RelayCommand(_ => {
            if (CurrentOrder == null) return;
            CurrentOrder.EditCommand.Execute(null);
        }));

        private ICommand _deleteCommand;
        public ICommand DeleteCommand => _deleteCommand ?? (_deleteCommand = new RelayCommand(_ => {
            if (CurrentOrder == null) return;
            CurrentOrder.DeleteCommand.Execute(null);
        }));

        private ICommand _deselectCommand;
        public ICommand DeselectCommand => _deselectCommand ?? (_deselectCommand = new RelayCommand(_ =>
        {
            CurrentOrder = null;
        }));

        private ICommand _backCommand;
        public ICommand BackCommand => _backCommand ?? (_backCommand = new RelayCommand(o =>{
            if (History.Count == 0) return;
            var tuple = History.Pop();
            if(tuple.Item2 != null)
                Future.Push(tuple);
            tuple.Item1?.Invoke();

        }));

        private ICommand _againCommand;
        public ICommand AgainCommand => _againCommand ?? (_againCommand = new RelayCommand(o => {
            if (Future.Count == 0) return;
            var tuple = Future.Pop();
            History.Push(tuple);
            tuple.Item2?.Invoke();

        }));

        private ICommand _saveCommand;
        public ICommand SaveCommand => _saveCommand ?? (_saveCommand = new RelayCommand(o =>{
            CurrentOrder = null;
            CheckSettings();
            if (ApplicationSetting.SavePrintBlackWhite)
            {
                GlobalColor = new ColorInfo("Schwarz", Colors.Black);
            }

            Application.Current.Dispatcher.Hooks.DispatcherInactive += SaveAsFile;
        }));

        #region saveAsFile
        private readonly object _saveLocker = new object();
        private bool _isSaving = false;
        private void SaveAsFile(object sender, EventArgs args)
        {
            lock (_saveLocker) { 
                if (_isSaving) return;
                _isSaving = true;
            }
            var rect = new Rect(_canvas.RenderSize);
            var rtb = new RenderTargetBitmap((int)rect.Right, (int)rect.Bottom, 96d, 96d, System.Windows.Media.PixelFormats.Default);
            rtb.Render(_canvas);
            //endcode as PNG
            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(rtb));

            //save to memory stream
            var ms = new System.IO.MemoryStream();

            pngEncoder.Save(ms);
            ms.Close();

            var dlg =
                new Microsoft.Win32.SaveFileDialog
                {
                    FileName = "Screenshot" + DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "",
                    DefaultExt = ".png",
                    Filter = "(.png)|*.png"
                };

            // Show save file dialog box
            var result = dlg.ShowDialog();

            // Process save file dialog box results
            Application.Current.Dispatcher.Hooks.DispatcherInactive -= SaveAsFile;
            if (result != true)
            {
                GlobalColor = null;
                _isSaving = false;
                return;
            }
            // Save document
            var filename = dlg.FileName;
            System.IO.File.WriteAllBytes(filename, ms.ToArray());
            GlobalColor = null;
            _isSaving = false;
        }
        #endregion

        private ICommand _printCommand;
        public ICommand PrintCommand => _printCommand ?? (_printCommand = new RelayCommand(o =>
        {
            CurrentOrder = null;
            CheckSettings();

            _print = new PrintDialog();
            if (_print.ShowDialog() != true) return;
            _print.PrintTicket.PageOrientation = PageOrientation.Landscape;
            _capabilities = _print.PrintQueue.GetPrintCapabilities(_print.PrintTicket);
            if (_capabilities.PageImageableArea == null){
                MessageBox.Show("Zugriff auf Drucker nicht möglich", "Hinweis", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (ApplicationSetting.SavePrintBlackWhite)
            {
                GlobalColor = new ColorInfo("Schwarz", Colors.Black);
            }

            Application.Current.Dispatcher.Hooks.DispatcherInactive += Print;
        }));

        #region print
        private readonly object _printLocker = new object();
        private PrintDialog _print;
        private PrintCapabilities _capabilities;
        private bool _isPrinting = false;
        private void Print(object sender, EventArgs args)
        {
            lock (_printLocker)
            {
                if (_isPrinting) return;
                _isPrinting = true;
            }

            if (_capabilities.PageImageableArea != null)
            {
                var scale = Math.Min(_capabilities.PageImageableArea.ExtentWidth / _canvas.ActualWidth,
                    _capabilities.PageImageableArea.ExtentHeight / _canvas.ActualHeight);

                var oldTransform = _canvas.LayoutTransform;

                _canvas.LayoutTransform = new ScaleTransform(scale, scale);

                var oldSize = new Size(_canvas.ActualWidth, _canvas.ActualHeight);
                var sz = new Size(_capabilities.PageImageableArea.ExtentWidth - 50,
                    _capabilities.PageImageableArea.ExtentHeight - 50);
                _canvas.Measure(sz);

                var heightDifference = (_capabilities.PageImageableArea.ExtentHeight - sz.Height) / 2;
                var widthDifference = (_capabilities.PageImageableArea.ExtentWidth - sz.Width) / 2;
                (_canvas).Arrange(new Rect(
                    new Point(_capabilities.PageImageableArea.OriginWidth + widthDifference,
                        _capabilities.PageImageableArea.OriginHeight + heightDifference),
                    sz));

                _print.PrintVisual(_canvas,
                    "Print" + DateTime.Now.Day + "" + DateTime.Now.Month + "" + DateTime.Now.Year + "");
                _canvas.LayoutTransform = oldTransform;
                _canvas.Measure(oldSize);

                (_canvas).Arrange(new Rect(new Point(10, 10),
                    oldSize));
            }
            else{
                MessageBox.Show("Zugriff auf Drucker nicht möglich", "Hinweis", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            Application.Current.Dispatcher.Hooks.DispatcherInactive -= Print;
            GlobalColor = null;
            _isPrinting = false;
        }
        #endregion

        private ICommand _selectCommand;
        public ICommand SelectCommand => _selectCommand ?? (_selectCommand = new RelayCommand(o =>
        {
            if (o == null) return;
            CurrentOrder = (OrderVm) o;
        }));

        private ICommand _moveWithMouseCommand;
        public ICommand MoveWithMouseCommand => _moveWithMouseCommand ?? (_moveWithMouseCommand = new RelayCommand(o =>
        {
            if (o == null) return;
            if (CurrentOrder == null) return;
            _uiElement = (FrameworkElement) o;
            _timer = new Timer {
                Interval = 2,
                Enabled = true
            };
            _timer.Elapsed += MoveOnMousePosition;
            _timer.AutoReset = true;
            _uiElement.MouseDown += _mouseDownEvent;
            _uiElement.MouseUp += _mouseUpEvent;
        }));

        private FrameworkElement _uiElement;
        private Timer _timer;
        private long _offset;

        private readonly object _locker = new object();
        private void MoveOnMousePosition(Object source, ElapsedEventArgs e)
        {
            lock (_locker) {
                if (CurrentOrder == null) return;
                var orderMiddlePointPosition = CurrentOrder.TotalWidth / 2 + CurrentOrder.LeftPosition;
                var virtualOrderMiddlePoint = orderMiddlePointPosition * _canvas.ActualWidth / ApplicationSetting.MachineWidth;

                var lastPosition = new Point(_window.ActualWidth - _canvas.ActualWidth + virtualOrderMiddlePoint, _window.ActualHeight - _canvas.ActualHeight / 2);
                Thread.Sleep(1);
                var currentPosition = MainWindow.GetCursorPosition();


                if (Math.Abs(lastPosition.X - currentPosition.X) < 10) return; 

                if (lastPosition.X > currentPosition.X)
                {
                    CurrentOrder.MoveKnifes(-10, false);
                    Interlocked.Add(ref _offset, 10);
                }
                else if (lastPosition.X < currentPosition.X)
                {
                    CurrentOrder.MoveKnifes(10, false);
                    Interlocked.Add(ref _offset, -10);
                }
            }
        }
        private void _mouseDownEvent(object sender, MouseEventArgs e)
        {
            if (e.RightButton != MouseButtonState.Pressed) return;
            _timer.Enabled = true;
        }
        private void _mouseUpEvent(object sender, MouseEventArgs e)
        {
            if (e.RightButton != MouseButtonState.Released) return;
            _timer.Enabled = false;
            _uiElement = null;

            if (Interlocked.Read(ref _offset) == 0) return;
            var x = Interlocked.Read(ref _offset);
            var y = -Interlocked.Read(ref _offset);
            lock (_locker) { 
                var refOrder = Orders[Orders.IndexOf(CurrentOrder)];
                History.Push(new Tuple<Action, Action>(() => {
                    refOrder.MoveKnifes(x, false);
                    }, () => {
                    refOrder.MoveKnifes(y, false);
                    }));
                Future.Clear();
            }
            Interlocked.Add(ref _offset, -_offset);
        }

        private ICommand _deleteCurrentCommand;
        public ICommand DeleteCurrentCommand => _deleteCurrentCommand ?? (_deleteCurrentCommand = new RelayCommand(o =>
        {
            if (CurrentOrder == null) return;
            CurrentOrder.Delete();
        }));

        #endregion
    }


    #region Sample
    
    public class SampleApplicationVm : ApplicationVm
    {
        public SampleApplicationVm() : base(null, null) { }
        public new ObservableCollection<OrderVm> Orders
        {
            get
            {
                var o = new ObservableCollection<OrderVm>();
                for (var i = 0; i < 10; i++)
                    o.Add(new OrderVm(new ApplicationVm(null, null), i, "adafdafd", CutTypeEnum.Beidseitig, false));
                return o;
            }
        }

        public new ObservableMulitSet<KnifeVm> Knifes { get; set; } = new ObservableMulitSet<KnifeVm>(10);

        public new OrderVm CurrentOrder { get; set; }

        public new ObservableCollection<Action> History => new ObservableCollection<Action>();

        public new ObservableCollection<double> KnifeDistances => new ObservableCollection<double>();
        public new bool IsStartingLeft => true;
        public new string IsStartingLeftText => "Links";
        public new ICommand AutoMoveCommand => new RelayCommand(o => { });
        public new ICommand NewOrderCommand => new RelayCommand(o => { });
        public new ICommand SettingsWindowCommand => new RelayCommand(o => { });
        public new ICommand CenterAllCommand => new RelayCommand(o => { });
        public new ICommand DeleteAllCommand => new RelayCommand(o => { });
        public new ICommand ShowHelpCommand => new RelayCommand(o => { });
        public new ICommand ShowInformationCommand => new RelayCommand(o => { });
        public new ICommand PreviousCommand => new RelayCommand(o => { });
        public new ICommand NextCommand => new RelayCommand(o => { });
        public new ICommand MoveLeftCommand => new RelayCommand(o => { });
        public new ICommand MoveRightCommand => new RelayCommand(o => { });
        public new ICommand MoveFarLeftCommand => new RelayCommand(o => { });
        public new ICommand MoveFarRightCommand => new RelayCommand(o => { });
        public new ICommand DeselectCommand => new RelayCommand(o => { });
        public new ICommand BackCommand => new RelayCommand(o => { });
        public new ICommand CenterCommand => new RelayCommand(o => { });
        public new ICommand EditCommand => new RelayCommand(o => { });
        public new ICommand DeleteCommand => new RelayCommand(o => { });
        public new ICommand SaveCommand => new RelayCommand(o => { });
        public new ICommand PrintCommand => new RelayCommand(o => { });
    }
    #endregion

}