![Logo](images/favicon.png)

# Messerwellenrechner

Diese Software unterliegt der beigelegten [BSD-Lizenz](licence.txt).

Der Messerwellenrechner ist eine C#-Neuimplementierung des Java-Originals und ist ein Werkzeug zur Berechnung der Positionen der Untermesser für mehrere Aufträge. Hierfür werden verschiedenste Funktionalitäten zur Verfügung gestellt, welche die Erstellung und die Positionierung von Aufträgen ermöglicht. Zudem können erstellte Pläne gespeichert sowie gedruckt und verschiedenste Einstellungen getroffen werden. Die Anwendung steht derzeit in der Version 1.0.3 ([Download](https://bitbucket.org/messerwellenrechnerteam/diameter-shaft-calculator/raw/438b3ceb9dd0f248f9e49cb11d28911f87b88661/download/Messerwellenrechner_V1_0_3.zip)) zur Verfügung.

![Logo](images/messerwellenrechner.PNG)

## Download
Nachfolgend finden Sie die Downloadmöglichkeit einer direkt ausführbaren Version der Applikation als .exe-Datei. 
Sollte der Download nicht automatisch starten sondern Sie auf eine andere Seite weiterführen, klicken Sie auf der nächsten Seite auf den "View raw" - Link.


[Download: 03.2018 - Version 1.0.3](https://bitbucket.org/messerwellenrechnerteam/diameter-shaft-calculator/raw/438b3ceb9dd0f248f9e49cb11d28911f87b88661/download/Messerwellenrechner_V1_0_3.zip)

## Einstellungen
Beim ersten Start der Anwendung öffnet sich das Einstellungsfenster, welches es ermöglichen die Anwendung individuell anzupassen. Hier kann die maximale Messeranzahl, der Mindestabstand zwischen einzelnen Messern, die Maschinenmitte, eine Standardnutzenbreite und weitere Einstellungen getroffen werden.

![Einstellungen](images/einstellungen.PNG)

## Aufträge
### Erstellen

Neue Aufträge können auf mehrere Weisen erstellt werden. Hierfür gibt es zum Einen den "+" Button im linken Bereich der Anwendung, sowie die "Neu"-Funktionalität im Aufträgemenü, welche auch über das Tastaturkürzel "N" erreicht werden kann.

Dadurch öffnet sich ein neuer Dialog indem ein neuer Auftrag erstellt werden kann. Hierfür kann eine Auftragsnummer, eine Beschreibung, die Beschnittart aber auch die Nutzenbreiten selbst definiert werden.

![Neuer Auftrag](images/neuerAuftrag.PNG)


Neu erstellte Aufträge werden nach einem Klick auf den "Speichern" Button im linken Bereich der Anwendung aufgelistet und im Plan eingezeichnet.

![Aufträge](images/auftraege.PNG)

### Einzeln bearbeiten

Erstellte Aufträge können auf verschiedenste Arten bearbeitet werden. Dies umfasst zum einen die Veränderung der im Erstellungsdialog definierten Eigenschaften über den "Ändern" Button, das löschen der Aufträge mithilfe des "Löschen" Buttons, das Zentrieren über den "Zentrieren" Button und die Positionsanpassung mithilfe der Pfeil-Buttons im Auftragsbereich im linken Teil der Anwedung, aber auch mithilfe der Pfeiltasten (Links und Rechts) auf der Tastatur und eines Rechtsklicks mithilfe des Mauszeigers. Für letztere beiden Möglichkeiten muss zunächst ein Auftrag gewählt werden. Dies ist durch einen Klick auf den Auftrag in der Liste oder mithilfe der Pfeiltasten (Oben und Unten) auf der Tastatur möglich. Bei gedrückter "Strg" Taste ist es zu dem Möglich die Position eines Auftrags in größeren Schritten zu verändern.

### Alle bearbeiten

Neben der Bearbeitung von einzelnen Aufträgen gibt es auch Funktionalitäten zur Veränderung aller vorhandenen Aufträge im "Aufträge" Menüpunkt. So ist es möglich alle Aufträge zu löschen (auch mit "Entf" möglich), alle Aufträge zu zentrieren ("C" auf der Tastatur) oder auch alle Aufträge automatisch so auszurichten ("A" auf der Tastatur), dass der eingestellte Minimalabstand eingehalten wird und der Verschub der einzelnen Aufträge minimal ist.

![Alle Aufträge bearbeiten](images/alleAuftraegeBearbeiten.png)

### Bearbeitunghistorie

Änderungen an Aufträgen werden mitprotokolliert und können über den Menüpunkt "Bearbeiten" rückgängig gemacht oder wiederholt werden. Dies ist zudem über die gewohnten Tastaturkürzel "Strg + Z" und "Strg + Y" möglich.

## Hilfe

Zusätzlich verfügt die Anwendung noch über eine Hilfe und eine Produktinformation, welche über das "Hilfe"-Menü erreicht werden kann.